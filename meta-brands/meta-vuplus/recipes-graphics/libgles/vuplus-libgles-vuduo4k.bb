require vuplus-libgles.inc

COMPATIBLE_MACHINE = "^(vuduo4k)$"

SRCDATE = "20190130"
SRCDATE_PR = "r0"
PV="18.1"

S = "${WORKDIR}/libgles-vuduo4k"

SRC_URI[md5sum] = "6609587f2687973b8fbfcfc1037c0d8c"
SRC_URI[sha256sum] = "9c7062f2cd02cb45acd0c30626718b7e4220a5be0f2c0422ddd5316ab4ff68ec"
